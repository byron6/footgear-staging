<?php
/**
 * Astra Child Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Astra Child
 * @since 1.0.0
 */

/**
 * Define Constants
 */
define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

/**
 * Enqueue styles
 */
function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css', array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );
	
	//wp_register_style( 'fg_css', get_stylesheet_directory_uri() . '/css/jquery.countdown.css' );
    //wp_enqueue_style( 'fg_css' );
	
	wp_register_script('fg_jquery', get_stylesheet_directory_uri(). '/js/fg-theme.js' ,array('jquery'),'', true);
    wp_enqueue_script('fg_jquery');
	
	//wp_register_script('countdown_jquery', get_stylesheet_directory_uri(). '/js/jquery.countdown.min.js' ,array('jquery'),'', true);
    //wp_enqueue_script('countdown_jquery');

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );

@ini_set( 'upload_max_size' , '1256M' );
@ini_set( 'post_max_size', '1256M');
@ini_set( 'max_execution_time', '300' );

function fg_cpt_funct() {
	
	register_post_type( 'ribbon-ads',
		array(
			'label' => __('Ribbon Ads'),
			'singular_label' => __('Ribbon Ad'),
			'exclude_from_search' => false,
			'public' => true,
			'show_ui' => true,
			'menu_icon' => 'dashicons-admin-post',
			'has_archive' => true,
			'hierarchical' => true,
			'show_in_nav_menus' => true,
			'rewrite' => array('slug' => 'ribbon-ads'),
			'supports' => array(
				'title',
				'revisions',
				'page-attributes',
				'custom-fields'
			),
        	'show_in_rest' => true
		)
	);
	
}
add_action('init', 'fg_cpt_funct');

//Footgear Ribbon - Ad Slider
function fg_ribbon_funct($atts){
	$html = '';
	
	$options = shortcode_atts( array(
		'count' => -1
	), $atts );
	
	$count = $options['count'];
	
	$args = array(
        'post_type'   => 'ribbon-ads',
        'post_status' => 'publish',
		'order'   => 'ASC',
        'posts_per_page' => $count
    );
	
    $the_query = new WP_Query( $args );	
	
	if( $the_query->have_posts() ){
		
		$now = date('Y-m-d H:i:s');
		
		$html .= '<div class="fg-ribbon">';
		
		foreach($the_query->posts as $portfolio) {
			$id = $portfolio->ID;
			$label = get_field('adv_label', $id);
			$start = get_field('adv_start', $id);
			$end = get_field('adv_end', $id);
			$countdown = get_field('adv_countdown', $id);
			$countdown_pos = get_field('adv_countdown_pos', $id);
			$link = get_field('adv_link', $id);
			$link_style = get_field('adv_link_style', $id);
			$link_pos = get_field('adv_link_pos', $id);
			
			$clock = '';
			$eLink = '';
			$bLink = '';
			$eLinkEnds = '';
			$class = '';
			
			if($now < $end && $start <= $now){
			
				if($countdown == 'yes'){
					$clock = '<div id="ribbon-clock" class="'.$countdown_pos.'" data-end="'.$end.'"></div>';
				}
				
				if(isset($link) && $link != ''){
					if($link_style == 'element'){
						$eLink = '<a href="'.$link.'">';
						$eLinkEnds = '</a>';
					}else{
						if($link_style == 'button'){
							$class = 'class="button"';
						}
						$bLink = '<div class="ribbon-link '.$link_pos.'"><a href="'.$link.'" '.$class.'>Learn More</a></div>';
					}
				}
				
				$html .= '<div class="ribbon-entry">'.$eLink.$clock.'<div class="ribbon-txt">'.$label.'</div>'.$bLink.$eLinkEnds.'</div>';		
			}
		}
		
		$html .= '</div>';
		
	}
	
	return $html;
}
add_shortcode('fg-ribbon', 'fg_ribbon_funct');