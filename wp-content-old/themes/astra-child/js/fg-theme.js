jQuery( document ).ready(function() {

	jQuery('.fg-ribbon').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
  		arrows: false,
		dots: false,
		autoplay: true,
		autoplaySpeed: 2000,
		infinite: true,
	});
	
	if(jQuery('#ribbon-clock').length){
		jQuery( '#ribbon-clock' ).each(function( index ) {
			
			var clock = jQuery(this);
			// Set the date we're counting down to
			var countDownDate = new Date(clock.attr('data-end')).getTime();
			console.log(countDownDate);
			
			// Update the count down every 1 second
			var x = setInterval(function() {
			
			  // Get today's date and time
			  var now = new Date().getTime();
				
			  // Find the distance between now and the count down date
			  var distance = countDownDate - now;
				
			  // Time calculations for days, hours, minutes and seconds
			  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			  var seconds = Math.floor((distance % (1000 * 60)) / 1000);
				
			  // Output the result in an element with id="demo"
			  clock.html(days + "d " + hours + "h " + minutes + "m " + seconds + "s ");
			  //document.getElementById("ribbon-clock").innerHTML = days + "d " + hours + "h "
			  //+ minutes + "m " + seconds + "s ";
				
			  // If the count down is over, write some text 
			  if (distance < 0) {
				clearInterval(x);
				clock.html("EXPIRED");
			  }
			}, 1000);
		
		});	
	}
	
});